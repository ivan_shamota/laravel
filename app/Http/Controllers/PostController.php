<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostFormRequest;
use App\Models\Post;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::where('status', 1)->orderBy('created_at', 'desc')->paginate(5);
        $title = "New posts";

        return view('home')->withPosts($posts)->withTitle($title);
    }

    public function create(Request $request)
    {
        if ($request->user()->canPost()) {
            return view('posts.create');
        } else {
            return redirect('/')->withErrors('You cannot create post');
        }
    }

    public function store(PostFormRequest $request)
    {
        $post = new Post();
        $post->title = $request->get('title');
        $post->text = $request->get('text');
        $post->author_id = $request->user()->id;
        $post->status = 1;

        $post->save();

        return redirect('/')->withMessage('Post created');
    }

    public function show($id)
    {
        $post = Post::where('id', $id)->first();
        if (!$post) {
            return redirect('/')->withErrors('Post not found');
        }

        $comments = $post->comments;

        return view('posts.show')->withPost($post)->withComments($comments);
    }

    public function edit($id, Request $request)
    {
        $post = Post::where('id', $id)->first();

        if ($post && ($request->user()->id == $post->author_id || $request->user()->isAdmin())) {
            return view('posts.edit')->withPost($post);
        }

        return redirect('/')->withErrors('You cannot edit post');
    }

    public function update($id, Request $request)
    {
        $post = Post::find($id);
        if ($post &&  ($post->author_id == $request->user()->id || $request->user()->isAdmin())) {
            $post->title = $request->input('title');
            $post->text = $request->input('text');
            $post->status = 1;
            $post->save();

            return redirect('/')->withMessage("Post updated");
        } else {
            return redirect('/')->withErrors('You cannot update post');
        }
    }

    public function destroy($id, Request $request)
    {
        $post = Post::find($id);
        if ($post && ($request->user()->id == $post->author_id || $request->user()->isAdmin())) {
            $post->delete();
            $data['success'] = 'Post deleted successfully';
        } else {
            $data['error'] = 'You cannot delete post';
        }

        return response()->json($data);
    }
}
