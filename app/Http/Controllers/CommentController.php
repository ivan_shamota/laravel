<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment['author_id'] = $request->user()->id;
        $comment['post_id'] = $request->input('post_id');
        $comment['text'] = $request->input('text');
        try {
            Comment::create($comment);
            $data['success'] = 'Comment added';
        } catch (\Exception $e) {
            $data['error'] = $e->getMessage();
        }

        return redirect("post/{$comment['post_id']}");
    }

    public function destroy($id, Request $request)
    {
        $comment = Comment::find($id);

        if ($comment && ($comment->author_id == $request->user()->id || $request->user()->isAdmin)) {
            $comment->delete();
            $data['success'] = 'Comment deleted';
        } else {
            $data['error'] = 'You cannot delete comment';
        }

        return response()->json($data);
    }
}
