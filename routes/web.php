<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PostController@index');
Route::get('/home', 'PostController@index');
Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('post/create', 'PostController@create');
    Route::post('post/create', 'PostController@store');
    Route::get('post/edit/{id}', 'PostController@edit');
    Route::put('post/edit/{id}', 'PostController@update');
    Route::delete('post/{id}', 'PostController@destroy');
    Route::get('post/{id}', 'PostController@show');

    Route::post('comment/add', 'CommentController@store');
    Route::delete('comment/{id}', 'CommentController@destroy');
    Route::get('auth/logout', 'Auth\LoginController@logout');
});
