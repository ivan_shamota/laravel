@extends('app')
@section('title')
    Add new post
@endsection

@section('content')
<form action="/post/create" method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <input type="text" name="title" placeholder="title" class="form-control" required="required">
    </div>
    <div class="form-group">
        <textarea id="text" class="form-control" name="text" placeholder="Content"></textarea>
    </div>
    <button class="btn btn-primary">Add post</button>
</form>
<script>
    $(document).ready(function(){
        $('#text').summernote();
    })
</script>
@endsection