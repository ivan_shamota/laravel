@extends('app')
@section('title')
    @if($post)
        {{ $post->title }}
        @if(!Auth::guest() && ($post->author_id == Auth::user()->id || Auth::user()->is_admin()))
            <button class="btn" style="float: right"><a href="{{ url('post/edit/'.$post->id)}}">Edit Post</a></button>
        @endif
    @else
        Page does not exist
    @endif
@endsection
@section('title-meta')
    <p>{{ $post->created_at->format('M d,Y \a\t h:i a') }} By <a href="{{ url('/user/'.$post->author_id)}}">{{ $post->author->name }}</a></p>
@endsection
@section('content')
    @if($post)
        <div>
            {!! $post->text !!}
        </div>
        <div>
            <h2 id="comments">Leave a comment</h2>
        </div>
        @if(Auth::guest())
            <p>Login to Comment</p>
        @else
            <div class="panel-body">
                <form method="post" action="/comment/add" id="comment_form">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                    <input type="hidden" name="author_id" value="{{ Auth::user()->id }}">
                    <div class="form-group">
                        <textarea required="required" placeholder="Enter comment here" name="text" class="form-control"></textarea>
                    </div>
                    <input type="submit" name='post_comment' class="btn btn-success"/>
                </form>
            </div>
        @endif
        <div id="comments_holder">
            @if($comments)
                <ul style="list-style: none; padding: 0">
                    @foreach($comments as $comment)
                        <li class="panel-body">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <h3>{{ $comment->author->name }}</h3>
                                    <p>
                                        <span class="text-left">
                                            {{ $comment->created_at->format('M d,Y \a\t h:i a') }}
                                        </span>
                                        <a href="#" data-comment="{{ $comment->id }}" class="btn btn-sm btn-danger pull-right remove-comment">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>
                                    </p>
                                </div>
                                <div class="list-group-item">
                                    <p>{{ $comment->text }}</p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    @else
        404 error
    @endif
    <script>
        $(document).ready(function() {
            $(document).on('click', '.remove-comment', function(e){
                e.preventDefault();
                var comment_id = $(this).data('comment');
                var token = $('#token')[0].value;
                var element = $(this);

                $.ajax({
                    url: '/comment/' + comment_id,
                    data: {
                        '_method': 'delete',
                        '_token': token
                    },
                    type: 'POST',
                    success: function(response) {
                        if (response.success) {
                            $(element).closest('.panel-body').remove();
                            alert('deleted');
                        }

                        if (response.error) {
                            alert(response.error);
                        }

                        $("html, body").animate({ scrollTop: $("#comment_form").offset().top }, "slow");
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });

            $('#comment_form').on('submit', function(e) {
                e.preventDefault();
                var comment = $(this).serialize();
                $.ajax({
                    type: 'POST',
                    url: '/comment/add',
                    data: comment,
                    success: function(response) {
                        $('#comments_holder').fadeOut(800, function() {
                            $('#comments_holder').load(window.location.href + " #comments_holder").fadeIn().delay(2000);
                        });
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });
        });
    </script>
@endsection