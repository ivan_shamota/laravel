@extends('app')
@section('title')
    Edit post
@endsection
@section('content')
    <form action="{{ url('post/edit/' . $post->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <input type="text" class="form-control" name="title" value="{{ $post->title }}" required="required">
        </div>
        <div class="form-group">
            <textarea id="text" class="form-control" name="text">
                {!! $post->text !!}
            </textarea>
        </div>
        <button class="btn btn-primary">Update</button>
    </form>

<script>
    $(document).ready(function(){
        $('#text').summernote();
    })
</script>
@endsection