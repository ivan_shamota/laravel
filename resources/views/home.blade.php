@extends('app')

@section('title')
    {{ $title }}
@endsection

@section('content')
    @forelse ($posts as $post)
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3>
                    <a href="{{ url("post/{$post->id}") }}">{{ $post->title }}</a>
                    @if (!Auth::guest())
                        <a href="#" data-post="{{ $post->id }}" class="btn btn-danger pull-right remove-post">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a>
                    @endif
                </h3>
            </div>
            <div class="panel-body">
                <article>
                    {!! str_limit($post->text, 1000, $end = '....... <a href='.url("/".$post->id).'>More</a>') !!}
                </article>
            </div>
            <div class="panel-footer">
                <p class="text-left">
                    Created by <a href="">{{ $post->author->name }}</a>
                    <br>
                    At <b>{{ $post->created_at->format('Y-m-d H:i:s') }}</b>
                    @if(!Auth::guest() && ($post->author_id == Auth::user()->id || Auth::user()->is_admin()))
                        <button class="btn btn-info" style="float: right"><a href="{{ url('post/edit/'.$post->id)}}">Edit Post</a></button>
                    @endif
                </p>
                <p>
                   <a href="{{ url("post/{$post->id}#comments") }}">Comments: <b>{{ $post->comments()->count() }}</b></a>
                </p>
            </div>
        </div>
    @empty
        <h3>There are no posts</h3>
    @endforelse
    {!! $posts->render() !!}
    <input type="hidden" id="token" value="{{ csrf_token() }}">
    <script>
        $(document).ready(function() {
            $('.remove-post').on('click', function(e) {
                e.preventDefault();
                var post_id = $(this).data('post');
                var token = $('#token')[0].value;
                var element = $(this);
                $.ajax({
                    url: 'post/' + post_id,
                    data: {
                        '_method': 'delete',
                        '_token': token
                    },
                    type: 'POST',
                    success: function(result) {
                        $('.alert').remove();
                        if (result.success) {
                            $(element).closest('.panel-info').remove();
                            var success_message = "<div class='alert alert-success'>Post deleted</div>";
                            $('.error-holder').empty();
                            $('.error-holder').append(success_message);
                        }

                        if (result.error) {
                            var error_message = "<div class='alert alert-danger'>This post cannot be deleted!</div>";
                            $('.error-holder').empty();
                            $('.error-holder').append(error_message);
                        }

                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });
        });
    </script>
@endsection